#!/usr/bin/env/ python3
import optparse
import scapy.all as scapy


def get_options_values():
    parser = optparse.OptionParser()
    parser.add_option(
        "-t", "--target", dest="target",
        help="IP address (e.g. 10.11.11.11) or network (e.g. 10.11.11.11/24) to scan"
    )
    (option_values, option) = parser.parse_args()

    if not option_values.target:
        parser.error("[-] Specify an IP address or network to scan. Type --help for more info")

    return option_values


def scan_clients(ip):
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request = scapy.ARP(pdst=ip)
    arp_request_broadcast = broadcast/arp_request
    answered = scapy.srp(arp_request_broadcast, timeout=1, verbose=False
                         )[0]
    clients = [
        {"ip": element[1].psrc, "mac": element[1].hwsrc}
        for element in answered
    ]

    return clients


def print_results(results_list):
    print("IP\t\t\tMAC Address")

    for client in results_list:
        print(f"{ client.get('ip') }\t\t{ client.get('mac') }")


options = get_options_values()
clients = scan_clients(options.target)
print_results(clients)
