# iptables -I FORWARD -j NFQUEUE --queue-num 0

import netfilterqueue

def process_packet(packet):
    print(packet)
    # forwards the packets
    packet.accept()
    # drops the packets
    #packet.drop()

queue = netfilterqueue.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()

# delete the iptables forward created:
# iptables --flush
